<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data General</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Logo Perusahaan
     </div>
     <div class='col-md-3'>
      <?php $hidden = ""; ?>
      <?php $hidden = isset($logo) ? 'hidden' : '' ?>
      <div class="<?php echo $hidden ?>" id="file_input">
       <input type="file" id="file" class="form-control" onchange="General.checkFile(this)">
      </div>

      <?php $hidden = ""; ?>
      <?php $hidden = isset($logo) ? '' : 'hidden' ?>
      <div class="<?php echo $hidden ?>" id="detail_file">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($logo) ? $logo : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-image hover-content" file="<?php echo isset($logo) ? $logo : '' ?>" 
            onclick="General.showLogo(this, event)"></i>         
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content"
            onclick="General.gantiFile(this, event)"></i>
        </span>
       </div>      
      </div>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Perusahaan
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='title' class='form-control required' 
             value='<?php echo isset($nama_perusahaan) ? $nama_perusahaan : '' ?>' error="Nama Perusahaan"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="General.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="General.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
