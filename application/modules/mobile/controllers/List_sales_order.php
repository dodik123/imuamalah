<?php

class List_sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'List_sales_order';
	}

	public function getTableName()
	{
		return 'invoice';
	}

	public function index()
	{
		echo 'List_sales_order';
	}

	public function getListSalesOrder()
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' i',
			'field' => array('i.*', 'p.nama as nama_pembeli', 
			'isa.status', 'pro.product as nama_product', 
			'sa.nama_satuan', 'ps.harga', 'isp.qty', "isp.id as invoice_product"),
			'join' => array(
				array('pembeli p', 'p.id = i.pembeli'),
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status isa', 'isa.id = iss.id'),
				array('(select max(id) id, invoice from invoice_product group by invoice) ispp', 'ispp.invoice = i.id'),
				array('invoice_product isp', 'isp.id = ispp.id'),
				array('product_satuan ps', 'ps.id = isp.product_satuan'),
				array('product pro', 'pro.id = ps.product'),
				array('satuan sa', 'sa.id = ps.satuan'),

			),
			'where' => "i.deleted is null or i.deleted = 0 and (i.createdby = '".$user."' or i.createdby is null)",
			'orderby' => "i.id desc"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = 'Rp, ' . number_format($value['total']);
				$value['produk'] = $value['nama_product'].' - '
				.$value['nama_satuan'].' - Rp, '.number_format($value['harga']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}
}
