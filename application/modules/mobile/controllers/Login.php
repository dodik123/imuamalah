<?php

class Login extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'Login';
 }

 public function getTableName() {
  return 'user';
 }

 public function index() {
  echo 'Login';
 }

 public function signIn(){
     $username = $_POST['username'];
    //  $username = 'bejo';
     $password = md5($_POST['password']);
    //  $password = md5('bejo');

     $data = Modules::run('database/get', array(
         'table'=> $this->getTableName().' u',
         'field'=> array('u.*', 'p.hak_akses'),
         'join' => array(
             array('priveledge p', 'u.priveledge = p.id')
         ),
         'where'=> "u.username = '".$username."' and u.password = '".$password."' 
         and u.deleted = 0 and p.hak_akses = 'sales'"
     ));

     $result = array();
     $message = "Username dan Password Tidak Valid";
     $is_valid = "0";
     if(!empty($data)){
        $result = $data->row_array();
        $is_valid = "1";
     }

     echo json_encode(array(
         'data'=> $result,
         'message'=> $message,
         'is_valid'=> $is_valid
     ));
 }
 
}
