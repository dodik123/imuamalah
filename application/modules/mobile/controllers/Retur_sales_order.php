<?php

class Retur_sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Retur_sales_order';
	}

	public function getTableName()
	{
		return 'return_penjualan';
	}

	public function index()
	{
		echo 'Sales_order';
	}

	public function getPostDataHeader()
	{
		$data['no_retur'] = Modules::run('no_generator/generateNoFakturReturPelanggan');
		$data['invoice'] = $_POST["invoice"];
		$data['tanggal_faktur'] = date('Y-m-d');
		$data['total'] = $_POST["total"];
		$data['keterangan'] = $_POST["keterangan"];
		$data['createddate'] = date('Y-m-d');
		$data['createdby'] = $_POST['user'];
		return $data;
	}

	public function prosesSimpan()
	{
		$is_valid = "0";

		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader();
			$this->db->insert($this->getTableName(), $post_data);
			$id = $this->db->insert_id();

			//invoice product item    
			$post_item['return_penjualan'] = $id;
			$post_item['invoice_product'] = $_POST['invoice_product'];
			$post_item['qty'] = $_POST['qty'];
			$post_item['sub_total'] = str_replace(',', '', $_POST['total']);

			$return_penjualan_item = Modules::run('database/_insert', 'return_penjualan_item', $post_item);


			//create jurnal akuntan
			$jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_retur']);
			$jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Retur Penjualan');
			//    echo '<pre>';
			//    print_r($jurnal_struktur);die;
			if (!empty($jurnal_struktur)) {
				foreach ($jurnal_struktur as $value) {
					$post_detail['jurnal'] = $jurnal;
					$post_detail['jurnal_struktur'] = $value['id'];
					$post_detail['jumlah'] = str_replace('.', '', $_POST['total']);
					Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
				}
			}
			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}
}
