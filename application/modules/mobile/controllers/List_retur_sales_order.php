<?php

class List_retur_sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'List_retur_sales_order';
	}

	public function getTableName()
	{
		return 'return_penjualan';
	}

	public function index()
	{
		echo 'List_retur_sales_order';
	}

	public function getListReturSalesOrder()
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' rp',
			'field' => array('rp.*', 'p.nama as nama_pembeli', 
			'isa.status', 'pro.product as nama_product', 
			'sa.nama_satuan', 'ps.harga', 'rpi.qty', 
			"isp.id as invoice_product", "i.no_faktur"),
			'join' => array(
				array('invoice i', 'i.id = rp.invoice'),
				array('(select max(id) id, return_penjualan from return_penjualan_item group by return_penjualan) rpp', 'rpp.return_penjualan = rp.id'),
				array('return_penjualan_item rpi', 'rpi.id = rpp.id'),
				array('pembeli p', 'p.id = i.pembeli'),
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status isa', 'isa.id = iss.id'),
				array('(select max(id) id, invoice from invoice_product group by invoice) ispp', 'ispp.invoice = i.id'),
				array('invoice_product isp', 'isp.id = ispp.id'),
				array('product_satuan ps', 'ps.id = isp.product_satuan'),
				array('product pro', 'pro.id = ps.product'),
				array('satuan sa', 'sa.id = ps.satuan'),

			),
			'where' => "rp.deleted is null or rp.deleted = 0 and (rp.createdby = '".$user."' or rp.createdby is null)",
			'orderby' => "rp.id desc"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = 'Rp, ' . number_format($value['total']);
				$value['produk'] = $value['nama_product'].' - '
				.$value['nama_satuan'].' - Rp, '.number_format($value['harga']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}
}
