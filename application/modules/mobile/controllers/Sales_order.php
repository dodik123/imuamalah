<?php

class Sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Sales_order';
	}

	public function getTableName()
	{
		return 'invoice';
	}

	public function index()
	{
		echo 'Sales_order';
	}

	public function getPostDataHeader()
	{
		$data['no_faktur'] = Modules::run('no_generator/generateNoFaktur');
		$data['pembeli'] = $_POST['pembeli'];
		$data['createdby'] = $_POST['user'];
		$data['tanggal_faktur'] = date('Y-m-d');
		$data['tanggal_bayar'] = date('Y-m-d');
		$data['createddate'] = date('Y-m-d');
		$data['total'] = str_replace('.', '', $_POST['total']);
		return $data;
	}

	public function prosesSimpan()
	{
		$is_valid = "0";

		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader();
			$this->db->insert($this->getTableName(), $post_data);
			$id = $this->db->insert_id();
			
			//invoice product item    
			$post_item['invoice'] = $id;
			$post_item['product_satuan'] = $_POST['product_satuan'];
			$post_item['metode_bayar'] = 1;
			$post_item['pajak'] = 1;
			$post_item['qty'] = $_POST['jumlah'];
			$post_item['sub_total'] = str_replace(',', '', $_POST['total']);

			$invoice_product = Modules::run('database/_insert', 'invoice_product', $post_item);
			

			//insert into product_log_stock
			$post_log_stok['product_satuan'] = $_POST['product_satuan'];
			$post_log_stok['status'] = 'OUT';
			$post_log_stok['qty'] = $_POST['jumlah'];
			$post_log_stok['keterangan'] = 'Sales Order';
			$log_stock = Modules::run('database/_insert', 'product_log_stock', $post_log_stok);			

			//    echo '<pre>';
			//    print_r($data_bank);die;
			//invoice status
			$post_status['invoice'] = $id;
			$post_status['user'] = $_POST['user'];
			$post_status['status'] = 'DRAFT';
			$invoice_status = Modules::run('database/_insert', 'invoice_status', $post_status);
			
			//invoice sisa
			$post_sisa['invoice'] = $id;
			$post_sisa['jumlah'] = str_replace('.', '', $_POST['total']);
			$invoice_sisa = Modules::run('database/_insert', 'invoice_sisa', $post_sisa);
			

			//create jurnal akuntan
			$jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
			$jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Faktur');
			//    echo '<pre>';
			//    print_r($jurnal_struktur);die;
			if (!empty($jurnal_struktur)) {
				foreach ($jurnal_struktur as $value) {
					$post_detail['jurnal'] = $jurnal;
					$post_detail['jurnal_struktur'] = $value['id'];
					$post_detail['jumlah'] = str_replace('.', '', $_POST['total']);
					Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
				}
			}
			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}
}
