<?php

class List_biaya_umum extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'List_biaya_umum';
	}

	public function getTableName()
	{
		return 'pembayaran_tagihan';
	}

	public function index()
	{
		echo 'List_biaya_umum';
	}

	public function getListBiayaUmum()
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' pt',
			'field' => array('pt.*', "t.tagihan as nama_tagihan"),
			'join' => array(
				array('tagihan t', 't.id = pt.tagihan'),

			),
			'orderby' => "pt.id desc"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = 'Rp, ' . number_format($value['total']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}
}
