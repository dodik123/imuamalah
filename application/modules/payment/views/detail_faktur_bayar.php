<div class='row'>
 <div class='col-md-12'>
  <u>Faktur Bayar</u>
 </div>
</div> 
<hr/>

<div class="row">
 <div class='col-md-3'>
  Tanggal Faktur
 </div>
 <div class='col-md-3'>
  <input disabled type='text' name='' readonly="" id='tanggal_faktur_fb' class='form-control required' 
         value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' error="Tanggal Faktur"/>
 </div>     
</div>
<br/>
<div class="row">
 <div class='col-md-3'>
  Tanggal Bayar
 </div>
 <div class='col-md-3'>
  <input disabled type='text' name='' readonly="" id='tanggal_bayar_fb' class='form-control required' 
         value='<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>' error="Tanggal Bayar"/>
 </div>     
</div>
<br/>   
<div class="row">
 <div class='col-md-3'>
  Jumlah
 </div>
 <div class='col-md-3'>
  <input disabled type='number' min="0" name='' id='jumlah' 
         class='form-control text-right required' 
         value='<?php echo isset($jumlah) ? $jumlah : '0' ?>' error="Jumlah"/>
 </div>     
</div>
<br/>   