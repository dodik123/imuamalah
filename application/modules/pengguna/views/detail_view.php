<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Hak Akses
     </div>
     <div class='col-md-3'>
      <?php echo $hak_akses ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Username
     </div>
     <div class='col-md-3'>
      <?php echo $username ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Pengguna.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
