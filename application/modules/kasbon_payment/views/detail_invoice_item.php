<table class="table table-striped table-bordered table-list-draft" id="tb_item">
 <thead>
  <tr class="bg-primary-light text-white">
   <th>No</th>
   <th>Faktur</th>
   <th class="text-center">Total (Rp)</th>
   <th class="text-center">Jumlah Bayar (Rp)</th>
   <th class="text-center">Total Belum Terbayar (Rp)</th>
   <th class="text-center">Status</th>
  </tr>
 </thead>
 <tbody>  
  <?php if (!empty($invoice_item)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($invoice_item as $value) { ?>  
    <tr data_id="<?php echo $value['id'] ?>">
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['no_kasbon'] ?></td>
     <td class="text-right"><?php echo number_format($value['invoice_total']) ?></td>
     <td class="text-right"><?php echo number_format($value['jumlah_bayar']) ?></td>
     <td class="text-right"><?php echo number_format($value['sisa_hutang']) ?></td>
     <td class="text-center"><?php echo $value['status'] ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr data_id="">
    <td colspan="7"class="text-center">
     Tidak ada data ditemukan
    </td>
   </tr>
  <?php } ?>  
 </tbody>
</table>