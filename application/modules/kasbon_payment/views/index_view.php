<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      <button class="btn btn-success" id="" onclick="KasbonPayment.add()">Tambah</button>
     </div>
     <div class="col-md-9">
      <div class="input-group">
       <input type="text" class="form-control" onkeyup="KasbonPayment.search(this, event)" id="keyword" placeholder="Pencarian">
       <span class="input-group-addon"><i class="fa fa-search"></i></span>
      </div>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>No</th>
         <th>No Faktur Bayar</th>
         <th>Tanggal</th>
         <th>Tanggal Bayar</th>
         <th>Total Bayar</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no']+1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['no_faktur'] ?></td>
           <td class="text-center"><?php echo $value['tanggal_faktur'] ?></td>
           <td class="text-center"><?php echo $value['tanggal_bayar'] ?></td>
           <td class="text-center"><?php echo 'Rp, '.number_format($value['jumlah']) ?></td>
           <td class="text-center">
<!--            <button id="" class="btn btn-warning-baru font12" 
                    onclick="KasbonPayment.ubah('<?php echo $value['id'] ?>')">Ubah</button>-->
            <!--&nbsp;-->
            <i class="fa fa-trash grey-text hover" onclick="KasbonPayment.delete('<?php echo $value['id'] ?>')"></i>
            &nbsp;
            <i class="fa fa-file-text grey-text  hover" onclick="KasbonPayment.detail('<?php echo $value['id'] ?>')"></i>
            
<!--            <button id="" class="btn btn-succes-baru font12" 
                    onclick="KasbonPayment.detail('<?php echo $value['id'] ?>')">Detail</button>
            &nbsp;
            <button id="" class="btn btn-danger-baru font12" 
                    onclick="KasbonPayment.delete('<?php echo $value['id'] ?>')">Hapus</button>
            &nbsp;-->
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="10" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>          
    </div> 
    <div class="row">
     <div class="col-md-12">
      <div class="pagination">
       <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>       
   </div>
  </div>
 </div>
</div>
