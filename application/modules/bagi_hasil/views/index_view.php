<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      <input type='text' name='' id='date' class='form-control' value='<?php echo date('Y-m-d') ?>'/>            
     </div>
     <div class="col-md-3">
      <button id="" class="btn btn-success" onclick="BagiHasil.search(this)">Proses</button>
     </div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Keterangan</th>
          <th>Presentase</th>
          <th>Laba Bersih</th>
          <th>Bagi Hasil</th>
         </tr>
        </thead>
        <tbody>
         <?php $total = 0; ?>
         <?php foreach ($internal as $value) { ?>
          <tr>
           <td><?php echo $value['keterangan'] ?></td>
           <td><?php echo $value['presentase'] . ' %' ?></td>
           <td><?php echo 'Rp. ' . number_format($hasil_total, 2, ',', '.') ?></td>
           <?php
           $total = (($hasil_total * $value['presentase']) / 100);
           ?>
           <td><?php echo 'Rp. ' . number_format($total, 2, ',', '.') ?></td>
          </tr>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>
  </div>
 </div>
</div>
