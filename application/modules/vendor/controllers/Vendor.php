<?php

class Vendor extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'vendor';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/vendor.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'vendor';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Vendor";
  $data['title_content'] = 'Data Vendor';
  $content = $this->getDataVendor();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataVendor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('v.nama', $keyword),
   array('v.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' v',
  'field' => array('v.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "v.deleted = 0 or v.deleted is null"
  ));

  return $total;
 }

 public function getDataVendor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('v.nama', $keyword),
   array('v.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' v',
  'field' => array('v.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "v.deleted = 0 or v.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataVendor($keyword)
  );
 }

 public function getDetailDataVendor($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Vendor";
  $data['title_content'] = 'Tambah Vendor';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataVendor($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Vendor";
  $data['title_content'] = 'Ubah Vendor';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataVendor($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Vendor";
  $data['title_content'] = 'Detail Vendor';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama_vendor'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  $data['alamat'] = $value->alamat;
  $data['keterangan'] = $value->keterangan;
  $data['email'] = $value->email;
  $data['status'] = $value->status;
  $data['vendor_category'] = 2;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $vendor = $id;
  $this->db->trans_begin();
  try {
   $post_vendor = $this->getPostDataHeader($data);
   if ($id == '') {
    $vendor = Modules::run('database/_insert', $this->getTableName(), $post_vendor);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_vendor, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'vendor' => $vendor));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Vendor";
  $data['title_content'] = 'Data Vendor';
  $content = $this->getDataVendor($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
