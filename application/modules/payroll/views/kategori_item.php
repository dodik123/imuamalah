<td>
 <select class="form-control required" id="payment_cat_<?php echo $index ?>" 
         error="Periode" onchange="Payroll.hitungTotal()">
  <option value="">Pilih Kategori</option>
  <?php if (!empty($list_kategori)) { ?>
   <?php foreach ($list_kategori as $value) { ?>
    <?php $selected = '' ?>
    <option action="<?php echo $value['action'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td>
 <input type="number" onkeyup="Payroll.hitungTotal()" value="0" min="0" id="jumlah_<?php echo $index ?>" class="form-control text-right required" error="Jumlah"/>
</td>
<td>
 <textarea id="keterangan_<?php echo $index ?>" class="form-control"></textarea>
</td>
<td>
 <i class="mdi mdi-delete mdi-18px" onclick="Payroll.deleteItem(this)"></i>
</td>

<script>
 $(function () {
  $('#payment_cat_' +<?php echo $index ?>).select2();

//  $('#jumlah_<?php echo $index ?>').divide({
//   delimiter: '.',
//   divideThousand: true
//  });
 });
</script>