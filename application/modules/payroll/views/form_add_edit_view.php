<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Pegawai
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="pegawai" 
              error="Pegawai">
       <option value="">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($pegawai)) { ?>
          <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <input type='text' name='' readonly="" id='tanggal_faktur' class='form-control required' 
             value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' error="Tanggal Faktur"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <input type='text' name='' readonly="" id='tanggal_bayar' class='form-control required' 
             value='<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>' error="Tanggal Bayar"/>
     </div>     
    </div>
    <br/>        
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Periode
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="periode" 
              error="Periode">
       <option value="">Pilih Periode</option>
       <?php if (!empty($list_periode)) { ?>
        <?php foreach ($list_periode as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($periode)) { ?>
          <?php $selected = $periode == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['month_str'].$value['year'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea id='keterangan' class='form-control' 
             value='<?php echo isset($keterangan) ? $keterangan : '' ?>'></textarea>
     </div>     
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Gaji</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive" id="content-product">
       <table class="table table-striped table-bordered table-list-draft" id="tb_item">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Kategori</th>
          <th>Jumlah</th>
          <th>Keterangan</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <tr data_id="">
          <td colspan="7"class="text-left">
           <a href="#" class="text-primary" onclick="Payroll.addItem(this, event)">Tambah Item</a>
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total" total="0"></label></h4>
     </div>
    </div>
    <br/>
    
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Payroll.simpan('<?php echo isset($id) ? $id : '' ?>')">Proses</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Payroll.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
