<?php

class Payroll_category extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'payroll_category';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/payroll_category.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'payroll_category';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategori";
  $data['title_content'] = 'Data Kategori';
  $content = $this->getDataKategori();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKategori($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.jenis', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  return $total;
 }

 public function getDataKategori($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.jenis', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKategori($keyword)
  );
 }

 public function getDetailDataKategori($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'where' => "p.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPlusMinus() {
  $result['+'] = "+ [Menambah]";
  $result['-'] = "- [Mengurangi]";
  
  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kategori";
  $data['title_content'] = 'Tambah Kategori';
  $data['list_action'] = $this->getListPlusMinus();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKategori($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kategori";
  $data['title_content'] = 'Ubah Kategori';
  $data['list_action'] = $this->getListPlusMinus();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataKategori($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kategori";
  $data['title_content'] = 'Detail Kategori';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['jenis'] = $value->jenis;
  $data['action'] = $value->action;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $palanggan = $id;
  $this->db->trans_begin();
  try {
   $post_pelanggan = $this->getPostDataHeader($data);
   if ($id == '') {
    $palanggan = Modules::run('database/_insert', $this->getTableName(), $post_pelanggan);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_pelanggan, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'pelanggan' => $palanggan));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategori";
  $data['title_content'] = 'Data Kategori';
  $content = $this->getDataKategori($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
