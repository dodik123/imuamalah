<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Order
     </div>
     <div class='col-md-3'>
      <?php echo $no_order ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pembeli ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>     
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Produk</th>
          <th>Jumlah</th>
          <th>Sub Total</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($order_item)) { ?>
          <?php foreach ($order_item as $value) { ?>
           <tr> 
            <td><?php echo $value['nama_product'] . '-' . $value['nama_satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
            <td><?php echo $value['qty'] ?></td>
            <td><?php echo number_format($value['sub_total']) ?></td>
           </tr>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
     </div>
    </div>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <?php if ($status != 'CANCEL') { ?>
       <button id="" class="btn btn-danger" onclick="Order.cancel('<?php echo isset($id) ? $id : '' ?>')">Cancel</button>
       &nbsp;
      <?php } ?>      
      <button id="" class="btn btn-baru" onclick="Order.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
