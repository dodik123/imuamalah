<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="pembeli" error="Pelanggan">
       <option value="">Pilih Pelanggan</option>
       <?php if (!empty($list_pelanggan)) { ?>
        <?php foreach ($list_pelanggan as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($pembeli)) { ?>
          <?php $selected = $pembeli == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <input type='text' name='' readonly="" id='tanggal_faktur' class='form-control required' 
             value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' error="Tanggal Faktur"/>
     </div>     
    </div>
    <br/>     
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Produk</th>
          <th>Jumlah</th>
          <th>Sub Total</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($invoice_item)) { ?>
          <?php $index = 0; ?>
          <?php foreach ($invoice_item as $value) { ?>
           <tr data_id="<?php echo $value['id'] ?>"> 
            <td>
             <select class="form-control select2 required" id="product<?php echo $index ?>" 
                     error="Produk" onchange="Order.hitungSubTotal(this, 'product')">
              <option value="" harga="0">Pilih Produk</option>
              <?php if (!empty($list_product)) { ?>
               <?php foreach ($list_product as $v_p) { ?>
                <?php $selected = $v_p['id'] == $value['product_satuan'] ? 'selected' : '' ?>
                <option harga="<?php echo $v_p['harga'] ?>" <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['nama_product'] . '-' . $v_p['satuan'] . '-[Rp, ' . number_format($v_p['harga']) . ']' ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <input type="number" value="<?php echo $value['qty'] ?>" min="1" id="jumlah" 
                    class="form-control text-right" 
                    onkeyup="Order.hitungSubTotal(this, 'jumlah')" 
                    onchange="Order.hitungSubTotal(this, 'jumlah')"/>
            </td>
            <td><?php echo number_format($value['sub_total']) ?></td>
            <td class="text-center">
             <i class="mdi mdi-delete mdi-18px" onclick="Order.deleteItem(this)"></i>
            </td>
           </tr>

           <?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
            <tr data_bank="<?php echo $value['bank'] ?>">
             <td colspan="7"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
            </tr>
           <?php } ?>

           <?php $index += 1; ?>
          <?php } ?>
         <?php } ?> 
         <tr data_id="">
          <td colspan="7">
           <label id="add_detail">
            <a href="#" onclick="Order.addItem(this, event)">Tambah Item</a>
           </label>
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Order.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Order.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
