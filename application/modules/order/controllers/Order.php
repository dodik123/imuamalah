<?php

class Order extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'order';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/order.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'order';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Order";
  $data['title_content'] = 'Data Order';
  $content = $this->getDataOrder();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataOrder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('o.no_order', $keyword),
       array('o.tanggal_faktur', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' o',
              'field' => array('o.*', 'isa.status'),
              'join' => array(
                  array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
                  array('order_status isa', 'isa.id = iss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "o.deleted is null or o.deleted = 0",
              'orderby' => 'o.id desc'
  ));

  return $total;
 }

 public function getDataOrder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('o.no_order', $keyword),
       array('o.tanggal_faktur', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' o',
              'field' => array('o.*', 'isa.status'),
              'join' => array(
                  array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
                  array('order_status isa', 'isa.id = iss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "o.deleted is null or o.deleted = 0",
              'orderby' => 'o.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataOrder($keyword)
  );
 }

 public function getDetailDataOrder($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' o',
              'field' => array('o.*',
                  'p.nama as nama_pembeli', 'isa.status'),
              'join' => array(
                  array('pembeli p', 'o.pembeli = p.id'),
                  array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
                  array('order_status isa', 'isa.id = iss.id'),
              ),
              'where' => "o.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array('ps.*', 'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product p', 'ps.product = p.id'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'where' => "ps.deleted = 0 or ps.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPelanggan() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null and p.pembeli_kategori = 2"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPajak() {
  $data = Modules::run('database/get', array(
              'table' => 'pajak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMetodeBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'metode_bayar',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Order";
  $data['title_content'] = 'Tambah Order';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function bayar() {
  $data['view_file'] = 'form_bayar';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Bayar Order";
  $data['title_content'] = 'Bayar Order';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataOrder($id);
//  echo $data['total'];die;
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Order";
  $data['title_content'] = 'Ubah Order';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['order_item'] = $this->getListInvoiceItem($id);
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataOrder($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Order";
  $data['title_content'] = 'Detail Order';
  $data['order_item'] = $this->getListInvoiceItem($id);
  echo Modules::run('template', $data);
 }

 public function getListInvoiceItem($order) {
  $data = Modules::run('database/get', array(
              'table' => 'order_product ip',
              'field' => array('ip.*', 'ps.satuan', 'ps.harga',
                  'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product_satuan ps', 'ps.id = ip.product_satuan'),
                  array('product p', 'p.id = ps.product'),
//                  array('bank b', 'b.id = ip.bank', 'left'),
//                  array('pajak pj', 'pj.id = ip.pajak'),
//                  array('metode_bayar m', 'm.id = ip.metode_bayar'),
                  array('satuan s', 's.id = ps.satuan', 'left')
              ),
              'where' => "ip.order = '" . $order . "' and ip.deleted = 0",
              'orderby' => 'ip.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_order'] = Modules::run('no_generator/generateNoOrder');
  $data['pembeli'] = $value->pembeli;
  $data['tanggal_faktur'] = date('Y-m-d', strtotime($value->tanggal_faktur));
  $data['total'] = str_replace('.', '', $value->total);
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //order product item    
    if (!empty($data->product_item)) {
     foreach ($data->product_item as $value) {
      $post_item['order'] = $id;
      $post_item['product_satuan'] = $value->product_satuan;
//      $post_item['metode_bayar'] = $value->metode;
//      $post_item['pajak'] = $value->pajak;
      $post_item['qty'] = $value->qty;
      $post_item['sub_total'] = str_replace(',', '', $value->sub_total);
//      if (trim($value->bank) != '') {
//       $post_item['bank'] = $value->bank;
//      } else {
//       unset($post_item['bank']);
//      }

      Modules::run('database/_insert', 'order_product', $post_item);

      //insert into product_log_stock
      $post_log_stok['product_satuan'] = $value->product_satuan;
      $post_log_stok['status'] = 'ORDER';
      $post_log_stok['qty'] = $value->qty;
      $post_log_stok['keterangan'] = 'Order Pelanggan';
      Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
     }
    }

//    echo '<pre>';
//    print_r($data_bank);die;
    //order status
    $post_status['order'] = $id;
//    $post_status['user'] = $this->session->userdata('user_id');
    $post_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'order_status', $post_status);
    //order sisa
//    $post_sisa['order'] = $id;
//    $post_sisa['jumlah'] = str_replace('.', '', $data->total);
//    Modules::run('database/_insert', 'order_sisa', $post_sisa);
    //create jurnal akuntan
//    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
//    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Order');
//
//    if (!empty($jurnal_struktur)) {
//     foreach ($jurnal_struktur as $value) {
//      $post_detail['jurnal'] = $jurnal;
//      $post_detail['jurnal_struktur'] = $value['id'];
//      $post_detail['jumlah'] = str_replace('.', '', $data->total);
//      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
//     }
//    }
   } else {
    //update
    unset($post_data['no_faktur']);
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));


    if (!empty($data->product_item)) {
     foreach ($data->product_item as $value) {
      $post_item['order'] = $id;
      $post_item['product_satuan'] = $value->product_satuan;
//      $post_item['metode_bayar'] = $value->metode;
//      $post_item['pajak'] = $value->pajak;
      $post_item['qty'] = $value->qty;
      $post_item['sub_total'] = str_replace(',', '', $value->sub_total);
//      if (trim($value->bank) != '') {
//       $post_item['bank'] = $value->bank;
//      } else {
//       unset($post_item['bank']);
//      }

      if ($value->id != '') {
       if ($value->deleted == 1) {
        $post_item['deleted'] = 1;
       } else {
        $post_item['deleted'] = 0;
       }

       Modules::run('database/_update', 'order_product', $post_item, array('id' => $value->id));
      } else {
       Modules::run('database/_insert', 'order_product', $post_item);
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Order";
  $data['title_content'] = 'Data Order';
  $content = $this->getDataOrder($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addItem() {
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['index'] = $_POST['index'];
  echo $this->load->view('product_item', $data, true);
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getMetodeBayar() {
  $data['list_bank'] = $this->getListBank();
  $data['index'] = $_POST['index'];
  echo $this->load->view('bank_akun', $data, true);
 }

 public function printOrder($id) {
  $data_pembayaran = array('data');
  $data = end($data_pembayaran['data']);

  $data['order'] = $this->getDetailDataOrder($id);
  $data['order_item'] = $this->getListInvoiceItem($id);
  $mpdf = Modules::run('mpdf/getInitPdf');

  $post_print['user'] = $this->session->userdata('user_id');
  $post_print['order'] = $id;
  Modules::run('database/_insert', 'order_print', $post_print);

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Customer - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function cancelOrder() {
  $order_id = $_POST['order_id'];
  $this->db->trans_begin();
  try {
   //order status
   $post_status['order'] = $order_id;
   $post_status['status'] = 'CANCEL';
   Modules::run('database/_insert', 'order_status', $post_status);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
