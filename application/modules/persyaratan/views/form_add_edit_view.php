<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Syarat
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='syarat' class='form-control required' 
             value='<?php echo isset($syarat) ? $syarat : '' ?>' error="Syarat"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Kategori Akad
     </div>
     <div class='col-md-3'>
      <select id="kategori_akad" error="Kategori Akad" class="form-control required">
       <?php if (!empty($list_kategori)) { ?>
        <?php foreach ($list_kategori as $v_k) { ?>
         <option <?php echo $v_k['id'] == $kategori_akad ? 'selected' : '' ?> value="<?php echo $v_k['id'] ?>"><?php echo $v_k['kategori'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Jenis Akad
     </div>
     <div class='col-md-3'>
      <select id="jenis_akad" error="Jenis Akad" class="form-control required">
       <?php if (!empty($list_jenis)) { ?>
        <?php foreach ($list_jenis as $v_j) { ?>
         <option <?php echo $v_j['id'] == $jenis_akad ? 'selected' : '' ?> value="<?php echo $v_j['id'] ?>"><?php echo $v_j['jenis'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea id="keterangan" class="form-control"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>     
    </div>
    <br/>    
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      <b><u>Data Berkas Persyaratan</u></b>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered" id="list_berkas">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>Jenis Berkas</th>
         <th class="text-center">Lampirkan Berkas</th>
         <th class="text-center">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!isset($detail)) { ?>
         <tr id="" >
          <td>
           <input type='text' name='' id='jenis_berkas' class='form-control required' value=''/>
          </td>
          <td class="text-center">
           <br/>
           <input type='checkbox' name='' id='check' class='' value=''/>
          </td>
          <td class="text-center">
           <i class="mdi mdi-plus mdi-24px hover" onclick="Persyaratan.addBerkas(this)"></i>
          </td>
         </tr>
        <?php } else { ?>
         <?php foreach ($detail as $value) { ?>
          <?php 
          $checked = "";
          if($value['status'] == 'Y'){
           $checked = 'checked';
          }
          ?>
          <tr id="<?php echo $value['id'] ?>">
           <td>
            <input type='text' name='' id='jenis_berkas' class='form-control required' value='<?php echo $value['nama_berkas'] ?>'/>
           </td>
           <td class="text-center">
            <input type='checkbox' <?php echo $checked ?> name='' id='check' class='form-control' value=''/>
           </td>
           <td class="text-center">
            &nbsp;
           </td>
          </tr>
         <?php } ?>

         <tr id="">
          <td>
           <input type='text' name='' id='jenis_berkas' class='form-control' value=''/>
          </td>
          <td class="text-center">
           <input type='checkbox' name='' id='check' class='form-control' value=''/>
          </td>
          <td class="text-center">
           <i class="mdi mdi-plus mdi-24px hover" onclick="Persyaratan.addBerkas(this)"></i>
          </td>
         </tr>
        <?php } ?>
       </tbody>
      </table>

     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Persyaratan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Persyaratan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
