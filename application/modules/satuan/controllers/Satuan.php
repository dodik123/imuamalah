<?php

class Satuan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'satuan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/satuan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'product_satuan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Satuan";
  $data['title_content'] = 'Data Satuan';
  $content = $this->getDataSatuan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataSatuan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
        array('p.product', $keyword),
       array('s.nama_satuan', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 
                  'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product p', 'k.product = p.id'),
                  array('satuan s', 's.id = k.satuan', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  return $total;
 }

 public function getDataSatuan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.product', $keyword),
       array('s.nama_satuan', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 
                  'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product p', 'k.product = p.id'),
                  array('satuan s', 's.id = k.satuan', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataSatuan($keyword)
  );
 }

 public function getDetailDataSatuan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product p', 'kr.product = p.id'),
                  array('satuan s', 's.id = kr.satuan', 'left')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSatuan() {
  $data = Modules::run('database/get', array(
              'table' => 'satuan',
              'where' => "deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Satuan";
  $data['title_content'] = 'Tambah Satuan';
  $data['list_product'] = $this->getListProduct();
  $data['list_satuan'] = $this->getListSatuan();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataSatuan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Satuan";
  $data['title_content'] = 'Ubah Satuan';
  $data['list_product'] = $this->getListProduct();
  $data['list_satuan'] = $this->getListSatuan();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataSatuan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Satuan";
  $data['title_content'] = 'Detail Satuan';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['product'] = $value->product;
  $data['satuan'] = $value->satuan;
  $data['level'] = $value->level;
  $data['harga'] = str_replace('.', '', $value->harga);
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  
  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Satuan";
  $data['title_content'] = 'Data Satuan';
  $content = $this->getDataSatuan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
