<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Satuan</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Produk
     </div>
     <div class='col-md-3'>
      <?php echo $nama_product ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Satuan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_satuan ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Harga
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, '.number_format($harga) ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Satuan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
