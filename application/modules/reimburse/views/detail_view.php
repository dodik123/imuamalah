<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Pegawai
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pegawai ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Jumlah
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, ' . number_format($jumlah) ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="Reimburse.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-danger-baru" onclick="Reimburse.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
