<?php

class Kerja_sama_internal extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kerja_sama_internal';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/kerja_sama_internal.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kerja_sama_internal';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kerja Sama Internal";
  $data['title_content'] = 'Data Kerja Sama Internal';
  $content = $this->getDataKerjaSama();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKerjaSama($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('i.keterangan', $keyword),
   array('i.presentase', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' i',
  'field' => array('i.*'),
  'like' => $like,
  'is_or_like' => true,
  'where'=> "i.deleted is null or i.deleted = 0"
  ));

  return $total;
 }

 public function getDataKerjaSama($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('i.keterangan', $keyword),
   array('i.presentase', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' i',
  'field' => array('i.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where'=> "i.deleted is null or i.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataKerjaSama($keyword)
  );
 }

 public function getDetailDataKerjaSama($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function Edit() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Edit";
  $data['title_content'] = 'Edit';

  $content = $this->getDataKerjaSama();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['keterangan'] = $value->keterangan;
  $data['presentase'] = $value->presentase;
  return $data;
 }

 public function getExistData($value) {
  $keterangan = $value->keterangan;
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName(),
  'where' => array('keterangan' => $keterangan)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   foreach ($data as $value) {
    $post_internal = $this->getPostDataHeader($value);
    $id = isset($value->id) ? $value->id : '';
    if ($value->keterangan != '' && $value->presentase != '') {
     if ($id == '') {
      $is_exist_data = $this->getExistData($value);
      if (!$is_exist_data) {
       Modules::run('database/_insert', $this->getTableName(), $post_internal);
       $is_valid = true;
      }else{
       $is_valid = false;
       $message = "Data Sudah Ada";
      }
     } else {
      //update
      Modules::run('database/_update', $this->getTableName(), $post_internal, array('id' => $id));
      $is_valid = true;
     }
    }
   }
   $this->db->trans_commit();   
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'message'=> $message));
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }
}
