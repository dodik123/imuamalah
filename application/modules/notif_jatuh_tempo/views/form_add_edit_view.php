<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Notif</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Nama Notif
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama_notif' class='form-control required' 
             value='<?php echo isset($nama_notif) ? $nama_notif : '' ?>' error="Nama Notif"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Jadwal Notif
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='jadwal_notif' class='form-control required' 
             value='<?php echo isset($jadwal_notif) ? $jadwal_notif : '' ?>' error="Jadwal Notif"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Jenis Notif
     </div>
     <div class='col-md-3'>
      <select id="jenis_notif" error="Jenis Notif" class="form-control required">
       <?php if (isset($jenis_notif)) { ?>
        <option value="+" <?php echo $jenis_notif == '+' ? 'selected' : '' ?>>+</option>        
        <option value="-" <?php echo $jenis_notif == '-' ? 'selected' : '' ?>>-</option>        
       <?php } else { ?>
        <option value="+">+</option>        
        <option value="-">-</option>        
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <u>Jenis Pengiriman</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>Pengiriman</th>
         <th class="text-center">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php foreach ($jenis_pengiriman as $v_p) { ?>
         <tr>
          <td><?php echo $v_p['jenis'] ?></td>
          <td class="text-center">
           <?php $check = ""; ?>
           <?php $id = ""; ?>
           <?php if (isset($detail)) { ?>
            <?php foreach ($detail as $v_d) { ?>
             <?php if ($v_d['jenis_notif_pengiriman'] == $v_p['id']) { ?>
              <?php $check = "checked" ?>
              <?php $id = $v_d['id']; ?>
             <?php } ?>
            <?php } ?>
           <?php } ?>
           <input id_jalur="<?php echo $id ?>" <?php echo $check ?> <?php echo $v_p['status'] == 1 ? '' : 'disabled' ?> type='checkbox' name='' id='check' class='' value='<?php echo $v_p['id'] ?>'/>
          </td>
         </tr>
        <?php } ?>
       </tbody>
      </table>

     </div>
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="JatuhTempo.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="JatuhTempo.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
