<?php

class Product_stock extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'product_stock';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/product_stock.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'product_stock';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Stok";
  $data['title_content'] = 'Data Stok';
  $content = $this->getDataStok();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataStok($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
        array('p.product', $keyword),
       array('k.stock', $keyword),
       array('s.nama_satuan', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.product as nama_product', 
                  'ps.satuan', 'g.nama_gudang', 
                  'r.nama_rak', 's.nama_satuan'),
              'join' => array(
                  array('product_satuan ps', 'k.product_satuan = ps.id'),
                  array('product p', 'ps.product = p.id'),
                  array('gudang g', 'k.gudang = g.id', 'left'),
                  array('rak r', 'k.rak = r.id', 'left'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  return $total;
 }

 public function getDataStok($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.product', $keyword),
       array('k.stock', $keyword),
       array('s.nama_satuan', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.product as nama_product', 
                  'ps.satuan', 'g.nama_gudang', 
                  'r.nama_rak', 's.nama_satuan'),
              'join' => array(
                  array('product_satuan ps', 'k.product_satuan = ps.id'),
                  array('product p', 'ps.product = p.id'),
                  array('gudang g', 'k.gudang = g.id', 'left'),
                  array('rak r', 'k.rak = r.id', 'left'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataStok($keyword)
  );
 }

 public function getDetailDataStok($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
             'field' => array('kr.*', 'p.product as nama_product', 'ps.satuan', 
                 'ps.product', 'r.nama_rak', 'g.nama_gudang'),
              'join' => array(
                  array('product_satuan ps', 'kr.product_satuan = ps.id'),
                  array('product p', 'ps.product = p.id'),
                  array('gudang g', 'kr.gudang = g.id', 'left'),
                  array('rak r', 'kr.rak = r.id', 'left'),
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListGudang() {
  $data = Modules::run('database/get', array(
              'table' => 'gudang p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListRak() {
  $data = Modules::run('database/get', array(
              'table' => 'rak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Stok";
  $data['title_content'] = 'Tambah Stok';
  $data['list_product'] = $this->getListProduct();
  $data['list_gudang'] = $this->getListGudang();
  $data['list_rak'] = $this->getListRak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataStok($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Stok";
  $data['title_content'] = 'Ubah Stok';
  $data['list_product'] = $this->getListProduct();
  $data['list_satuan'] = $this->getDataSatuan($data['product']);
  $data['list_gudang'] = $this->getListGudang();
  $data['list_rak'] = $this->getListRak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataStok($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Stok";
  $data['title_content'] = 'Detail Stok';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['product_satuan'] = $value->product_satuan;
  $data['stock'] = str_replace('.', '', $value->stock);
  if($value->gudang != ''){
   $data['gudang'] = $value->gudang;
  }
  if($value->rak != ''){
   $data['rak'] = $value->rak;
  }
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  
  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Stok";
  $data['title_content'] = 'Data Stok';
  $content = $this->getDataStok($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataSatuan($product) {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field'=> array('ps.*', 's.nama_satuan'),
              'join' => array(
                  array('satuan s', 'ps.satuan = s.id', 'left')
              ),
              'where' => "ps.deleted = 0 and ps.product = '".$product."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getSatuanProduk() {
  $product = $_POST['product'];
  $data_satuan = $this->getDataSatuan($product);
  $data['list_satuan'] = $data_satuan;
  echo $this->load->view('list_satuan', $data, true);
 }
}
