<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">
    <div class="row">
     <div class='col-md-3 text-bold'>
      Produk
     </div>
     <div class='col-md-3'>
      <?php echo $nama_product ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Satuan
     </div>
     <div class='col-md-3'>
      <?php echo $satuan ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Gudang
     </div>
     <div class='col-md-3'>
      <?php echo $nama_gudang ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Rak
     </div>
     <div class='col-md-3'>
      <?php echo $nama_rak ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Stok
     </div>
     <div class='col-md-3'>
      <?php echo number_format($stock, 2, ',', '.') ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="ProductStock.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
