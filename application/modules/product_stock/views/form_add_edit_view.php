<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Produk
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="product" 
              error="Produk" onchange="ProductStock.getSatuanProduk(this)">
       <option value="">Pilih Produk</option>
       <?php if (!empty($list_product)) { ?>
        <?php foreach ($list_product as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($product)) { ?>
          <?php $selected = $product == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['product'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Satuan
     </div>
     <div class='col-md-3' id="content-satuan">
      <select class="form-control required" id="product_satuan" 
              error="Satuan">
       <option value="">Pilih Satuan</option>
       <?php if (isset($list_satuan)) { ?>
        <?php if (!empty($list_satuan)) { ?>
         <?php foreach ($list_satuan as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($product_satuan)) { ?>
           <?php $selected = $product_satuan == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_satuan'] ?></option>
         <?php } ?>
        <?php } ?>
       <?php } ?>       
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Gudang
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="gudang" 
              error="Gudang">
       <option value="">Pilih Gudang</option>
       <?php if (isset($list_gudang)) { ?>
        <?php if (!empty($list_gudang)) { ?>
         <?php foreach ($list_gudang as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($gudang)) { ?>
           <?php $selected = $gudang == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_gudang'] ?></option>
         <?php } ?>
        <?php } ?>
       <?php } ?>       
      </select>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Rak
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="rak" 
              error="Rak">
       <option value="">Pilih Rak</option>
       <?php if (isset($list_rak)) { ?>
        <?php if (!empty($list_rak)) { ?>
         <?php foreach ($list_rak as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($rak)) { ?>
           <?php $selected = $rak == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_rak'] ?></option>
         <?php } ?>
        <?php } ?>
       <?php } ?>       
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Stok
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='stock' class='form-control text-right required' 
             value='<?php echo isset($stock) ? $stock : '0' ?>' error="Stok"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="ProductStock.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="ProductStock.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
