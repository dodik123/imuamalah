<td>
 <select class="form-control required" id="product<?php echo $index ?>" 
         error="Produk" onchange="FakturPelanggan.hitungSubTotal(this, 'product')">
  <option value="" harga="0">Pilih Produk</option>
  <?php if (!empty($list_product)) { ?>
   <?php foreach ($list_product as $value) { ?>
    <?php $selected = '' ?>
    <option harga="<?php echo $value['harga'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_product'] . '-' . $value['nama_satuan'] . '-[Rp, ' . number_format($value['harga']) . ']' ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td>
 <select class="form-control required" id="pajak<?php echo $index ?>" error="Pajak"
         onchange="FakturPelanggan.hitungSubTotal(this, 'pajak')">
  <option value="" persentase="0">Pilih Pajak</option>
  <?php if (!empty($list_pajak)) { ?>
   <?php foreach ($list_pajak as $value) { ?>
    <?php $selected = '' ?>
    <option persentase="<?php echo $value['persentase'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td>
 <select class="form-control required" id="metode<?php echo $index ?>" 
         error="Metode Bayar" onchange="FakturPelanggan.getMetodeBayar(this)">
  <option value="">Pilih Metode Bayar</option>
  <?php if (!empty($list_metode)) { ?>
   <?php foreach ($list_metode as $value) { ?>
    <?php $selected = '' ?>
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['metode'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td class="text-center">
 <input type="number" value="1" min="1" id="jumlah" 
        class="form-control text-right" 
        onkeyup="FakturPelanggan.hitungSubTotal(this, 'jumlah')" 
        onchange="FakturPelanggan.hitungSubTotal(this, 'jumlah')"/>
</td>      
<td class="text-center">
 <label id="sub_total">0</label>
</td>      
<td class="text-center">
 <i class="mdi mdi-delete mdi-18px" onclick="FakturPelanggan.deleteItem(this)"></i>
</td>


<script>
 $(function () {
  $("#product<?php echo $index ?>").select2();
  $("#metode<?php echo $index ?>").select2();
  $("#pajak<?php echo $index ?>").select2();
 });
</script>