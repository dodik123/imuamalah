<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo 'Invoice'; ?></title>

  <style>
   body {
    font-family: "Helvetica", sans-serif;
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .text-left {
    text-align: left;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 10px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }
  </style>
 </head>
 <body>
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="width: 50%">
      <table style="margin-bottom: 8px">
       <tr>
           <td width="40"><img src="<?php echo base_url() ?>files/berkas/general/<?php echo $self['logo'] ?>" alt="" width="40"></td> 
        <td>
         <div class="font-bold"><?php echo $self['title'] ?></div>
         <div><?php echo $self['alamat'] ?></div>
        </td>
       </tr>
       <tr>
           <!-- <td width="40"><img src="<?php echo base_url() ?>assets/images/logo/wjc-logo.png" alt="" width="40"></td> -->
        <td>
         <div class="font-bold" style="font-size: 14px; margin-top: 16px">FAKTUR</div>
        </td>
       </tr>
      </table>
      <table>
       <tr>
        <td width="100">Nomor Faktur</td>
        <td width="10">:</td>
        <td class="font-bold"><?php echo $invoice['no_faktur'] ?></td>
       </tr>
       <tr>
        <td colspan="3" style="padding-top: 8px">Detail Faktur sbb:</td>
       </tr>
      </table>
     </td>
     <td style="width: 50%">
      <table>
       <tr>
        <td class="text-right">
         <div class="font-bold">Tgl. Faktur</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo date("d F Y", strtotime($invoice['tanggal_faktur'])) ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold">Status</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $invoice['status'] ?></div>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </tbody>
  </table>
  <table class="table-item">
   <thead>
    <tr>
     <th>Produk</th>
     <th>Satuan</th>
     <th>Jumlah</th>
     <th>Pajak</th>
     <th>Metode Pemabayaran</th>
     <th width="125">Total Bayar</th>
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($invoice_item)) { ?>
     <?php foreach ($invoice_item as $value) { ?>
      <tr>
       <td><?php echo $value['nama_product'] ?></td>
       <td class="text-center"><?php echo $value['satuan'] ?></td>
       <td class="text-center"><?php echo $value['qty'] ?></td>
       <td class="text-center"><?php echo $value['jenis'] ?></td>
       <td class="text-center"><?php echo $value['metode'] ?></td>
       <td class="text-right"><?php echo 'Rp. ' . number_format($value['sub_total'], 2, ',', '.') ?></td>
      </tr>

      <?php if ($value['bank'] != '' && $value['bank'] != '0') { ?>
       <tr>
        <td class="text-left font-bold" colspan="6"><?php echo $value['nama_bank'].' - '.$value['no_rekening'].' - '.$value['akun'] ?></td>
       </tr>
      <?php } ?>
     <?php } ?>
    <?php } ?>    
    <tr>
     <td style="border: 0" class="text-right" colspan="5">Total</td>
     <td class="text-right font-bold"><?php echo 'Rp. ' . number_format($invoice['total'], 2, ',', '.') ?></td>
    </tr>
   </tbody>
  </table>
  <table style="width: 100%; margin-top: 32px">
   <tbody>
    <tr>
     <td class="text-center">Perusahaan,</td>
<!--     <td class="text-center">Supplier,</td>-->
     <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
    </tr>
    <tr>
     <td class="text-center">(------------------------------)</td>
     <!--<td class="text-center">(------------------------------)</td>-->
     <!--<td class="text-center">Hal:---------------------------------</td>-->
    </tr>
   </tbody>
  </table>
 </body>
</html>
