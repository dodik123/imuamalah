<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class="col-md-4">
      &nbsp;      
     </div>
     <div class="col-md-4">
      &nbsp;      
     </div>
     <div class="col-md-4">
      <?php if ($approve != '') { ?>
       <?php echo $this->load->view('pesan') ?>
      <?php } ?>      
     </div>
    </div>    
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $no_faktur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pembeli ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_bayar ?>
     </div>     
    </div>
    <br/>        
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Produk</th>
          <th>Pajak</th>
          <th>Metode Bayar</th>
          <th>Jumlah</th>
          <th>Sub Total</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($invoice_item)) { ?>
          <?php foreach ($invoice_item as $value) { ?>
           <tr> 
            <td><?php echo $value['nama_product'] . '-' . $value['nama_satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
            <td>
             <?php if ($value['persentase'] != '0') { ?>
              <?php echo $value['jenis'] ?>
             <?php } else { ?>
              <?php echo $value['jenis'] ?>
             <?php } ?>
            </td>
            <td><?php echo $value['metode'] ?></td>
            <td><?php echo $value['qty'] ?></td>
            <td><?php echo number_format($value['sub_total']) ?></td>
           </tr>

           <?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
            <tr>
             <td colspan="7" class="text-primary"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
            </tr>
           <?php } ?>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
     </div>
    </div>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="FakturPelanggan.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="FakturPelanggan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
