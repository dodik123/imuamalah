<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Gudang
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama_gudang' class='form-control required' 
             value='<?php echo isset($nama_gudang) ? $nama_gudang : '' ?>' error="Nama Gudang"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Gudang.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Gudang.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
