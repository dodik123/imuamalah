<header id="header" class="header">
 <div class="header-logo text-center">  
<!--  <img width="36" height="36" class="align-content" src="<?php echo base_url() ?>assets/images/drugs.png" alt="Logo Menu">
  <a class="navbar-brand" href="javascript:;" style="margin-left:16px;color:#ffffff;font-family: robotoFonts">
   Smart Apotek</a>
  <a class="navbar-brand hidden">&nbsp;</a>-->
  <a class="navbar-brand" href="javascript:;" style="margin-right: 0;color:#000;">
   <img src="<?php echo base_url().'assets/images/logo/I-Muamalah-logo.png' ?>" id="logo"/></a>
  <a class="navbar-brand hidden">&nbsp;</a>
 </div>
 <div class="header-menu">
  <div class="col-sm-7">
   <a id="menuToggle" class="menutoggle pull-left"><i style="margin-top: 12px;" class="fa fa fa-tasks"></i></a>
   <div class="header-left">
    <label style="margin-top: 18px;"><?php echo isset($general_title) ? $general_title : 'IMuamalah' ?></label>
   </div>
  </div>

  <div class="col-sm-5" style="padding-right: 0;">
   <ul class="nav justify-content-end">
    <li class="nav-item" style="margin-right: 24px;">
     <div class="user-area dropdown float-right inf-shake">

     </div>
    </li>
    <li>
     <div class="user-area dropdown float-right">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       <?php echo strtoupper($this->session->userdata('username'))?>
       <i class="fa fa-user-circle fa-lg fa-2x" style="margin-top: 8px;margin-bottom: 8px;"></i>
      </a>

      <div class="user-menu dropdown-menu" style="background-color: #dee2e6 !important;box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.15);">
       <a class="nav-link" onclick="Template.changePassword(this, event)" style="font-family: robotoFonts;" href=""><i class="mdi mdi-key fa-lg"></i>  Ganti Password</a>
       <a class="nav-link" style="font-family: robotoFonts;" href="<?php echo base_url() ?>login/sign_out"><i class="mdi mdi-power fa-lg"></i>  Keluar</a>
      </div>
     </div>
    </li>
   </ul>
  </div>
 </div>

</header><!-- /header -->
