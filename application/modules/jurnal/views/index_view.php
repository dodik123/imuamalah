<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-12">
      <div class="input-group">
       <input type="text" class="form-control" onkeyup="Jurnal.search(this, event)" id="keyword" placeholder="Pencarian">
       <span class="input-group-addon"><i class="fa fa-search"></i></span>
      </div>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>No</th>
         <th>No Jurnal</th>
         <th>No Ref</th>
         <th>Transaksi</th>
         <th>Kode Akun</th>
         <th>Akun</th>
         <th>Jumlah</th>
         <th>Jenis</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>        
         <?php $temp = "" ?>        
         <?php foreach ($content as $value) { ?>        
          <tr>
           <td><?php echo $value['no_jurnal'] == $temp ? '' : $no++ ?></td>
           <td class="text-bold"><?php echo $value['no_jurnal'] == $temp ? '' : $value['no_jurnal'] ?></td>
           <td><?php echo $value['no_jurnal'] == $temp ? '' : $value['ref'] ?></td>
           <td><?php echo $value['transaksi'] ?></td>
           <td><?php echo $value['code'] ?></td>
           <td><?php echo $value['akun'] ?></td>
           <td class="text-bold"><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
           <?php $bg_color = $value['jenis'] == 'DEBIT' ? 'text-success' : 'text-danger' ?>
           <td class="<?php echo $bg_color ?>"><?php echo $value['jenis'] ?></td>
          </tr>
          <?php $temp = $value['no_jurnal'] ?>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>          
    </div> 
   </div>

   <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </div>
  </div>
 </div>
</div>
