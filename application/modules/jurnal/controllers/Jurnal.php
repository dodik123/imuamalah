<?php

class Jurnal extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'jurnal';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/jurnal.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'jurnal';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jurnal";
  $data['title_content'] = 'Data Jurnal';
  $content = $this->getDataJurnal();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataJurnal($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('j.no_jurnal', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' j',
               'field' => array('j.*', 
                  'f.nama as transaksi', 'c.code', 
                  'c.keterangan as akun', 'ct.jenis', 'jd.jumlah'),
              'join' => array(
                  array('jurnal_detail jd', 'jd.jurnal = j.id'),
                  array('jurnal_struktur js', 'jd.jurnal_struktur = js.id'),
                  array('feature f', 'f.id = js.feature'),
                  array('coa c', 'c.id = js.coa'),
                  array('coa_type ct', 'ct.id = js.coa_type'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "j.deleted is null or j.deleted = 0"
  ));

  return $total;
 }

 public function getDataJurnal($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('j.no_jurnal', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' j',
              'field' => array('j.*', 
                  'f.nama as transaksi', 'c.code', 
                  'c.keterangan as akun', 'ct.jenis', 'jd.jumlah'),
              'join' => array(
                  array('jurnal_detail jd', 'jd.jurnal = j.id'),
                  array('jurnal_struktur js', 'jd.jurnal_struktur = js.id'),
                  array('feature f', 'f.id = js.feature'),
                  array('coa c', 'c.id = js.coa'),
                  array('coa_type ct', 'ct.id = js.coa_type'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "j.deleted is null or j.deleted = 0",
              'orderby'=> 'j.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataJurnal($keyword)
  );
 }

 public function getDetailDataJurnal($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'cp.keterangan as nama_parent'),
              'join' => array(
                  array('coa cp', 'kr.parent = cp.id', 'left')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListParent() {
  $data = Modules::run('database/get', array(
              'table' => 'coa',
              'where' => "deleted = 0 and parent is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Jurnal";
  $data['title_content'] = 'Tambah Jurnal';
  $data['list_parent'] = $this->getListParent();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataJurnal($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Jurnal";
  $data['title_content'] = 'Ubah Jurnal';
  $data['list_parent'] = $this->getListParent();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataJurnal($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Jurnal";
  $data['title_content'] = 'Detail Jurnal';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  if ($value->parent != '') {
   $data['parent'] = $value->parent;
  }
  $data['keterangan'] = $value->akun;
  $data['code'] = $value->code;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jurnal";
  $data['title_content'] = 'Data Jurnal';
  $content = $this->getDataJurnal($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
