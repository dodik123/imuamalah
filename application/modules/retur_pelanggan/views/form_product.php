<div class="row">
 <div class="col-md-10">
  <div class="table-responsive">
   <table class="table table-striped table-bordered table-list-draft" id="tb_product">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Produk</th>
      <th>Satuan</th>
      <th>Harga</th>
      <th>Jumlah</th>
      <th>Sub Total</th>
      <th>Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($invoice_item)) { ?>
      <?php $index = 0; ?>
      <?php $temp = ''; ?>
      <?php foreach ($invoice_item as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>"> 
        <td>
         <?php echo $value['nama_product'] == $temp ? '' : $value['nama_product'] ?>
        </td>
        <td>
         <?php echo $value['nama_satuan'] ?>
        </td>
        <td harga="<?php echo $value['harga'] ?>">
         <?php echo 'Rp, '. number_format($value['harga']) ?>
        </td>
        <td>
         <input type="number" value="<?php echo $value['qty'] ?>" min="1" max="<?php echo $value['qty'] ?>" id="jumlah" 
                class="form-control text-right" 
                onkeyup="ReturPelanggan.hitungSubTotal(this, 'jumlah')" 
                onchange="ReturPelanggan.hitungSubTotal(this, 'jumlah')"/>
        </td>
        <td><?php echo number_format($value['sub_total']) ?></td>
        <td class="text-center">
         <input type="checkbox" value="" id="check" class="" onchange="ReturPelanggan.hitungSubTotalRetur(this)"/>
        </td>
       </tr>
       <?php $index += 1; ?>
       <?php $temp = $value['nama_product']; ?>
      <?php } ?>
     <?php } ?> 
    </tbody>
   </table>
  </div>
 </div>
</div>