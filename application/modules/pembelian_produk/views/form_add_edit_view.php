<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Customer</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      List Customer
     </div>
     <div class='col-md-3'>
      <select id="customer" error="Customer" class="form-control required" onchange="PembelianProduk.getCustomer(this)">
       <option value="">--Pilih Customer--</option>       
       <option value="baru">(+) Customer Baru</option>
       <?php if (!empty($list_customer)) { ?>
        <?php foreach ($list_customer as $v_cus) { ?>
         <option value="<?php echo $v_cus['id'] ?>"><?php echo $v_cus['nama'] ?></option>
        <?php } ?>
       <?php } else { ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class='data_customer hide'>
     <div class="row">
      <div class='col-md-3'>
       Nama Customer
      </div>
      <div class='col-md-3'>
       <input type='text' name='' id='nama_customer' class='form-control required' 
              value='<?php echo isset($nama_customer) ? $nama_customer : '' ?>' error="Nama Customer"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       No HP
      </div>
      <div class='col-md-3'>
       <input type='text' name='' id='no_hp' class='form-control required' 
              value='<?php echo isset($no_hp) ? $no_hp : '' ?>' error="No HP"/>
      </div>     
     </div>
     <br/>
    
     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-3'>
       <input type='text' name='' id='email' class='form-control required' 
              value='<?php echo isset($emal) ? $emal : '' ?>' error="Email"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alamat
      </div>
      <div class='col-md-3'>
       <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
      </div>     
     </div>
     <br/>
    </div>
    <hr/>

    <div class='row'>
     <div class='col-md-12'>
      <u>Data Produk</u>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-5">
      <div class="form-inside-icon icon-pos-right">
       <input type="text" id="keyword" class="form-control" placeholder="Pencarian" onkeyup="PembelianProduk.searchInTable(this)">
       <div class="form-icon">
        <i class="fa fa-search"></i>
       </div>
      </div>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id='list_product'>
       <thead>
        <tr>
         <th>No</th>
         <th>Produk</th>
         <th>Tipe</th>
         <th>Kategori</th>
         <th class="text-center">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_product)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_product as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['product'] ?></td>
           <td><?php echo $value['tipe'] ?></td>
           <td><?php echo $value['kategori'] ?></td>
           <td class="text-center">
            <button id="" class="btn btn-succes-baru font12" 
                    onclick="PembelianProduk.detailProduk('<?php echo $value['id'] ?>')">Detail</button>
            &nbsp;
            <button id="" class="btn btn-info-baru font12" 
                    onclick="PembelianProduk.listFotoProduk('<?php echo $value['id'] ?>')">List Foto Produk</button>
            &nbsp;
            <button id="" class="btn btn-warning-baru font12" 
                    onclick="PembelianProduk.addProduk('<?php echo $value['id'] ?>', this)">Ambil</button>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <u>Produk yang Diambil</u>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id="list_product_taken">
       <thead>
        <tr>
         <th>Produk</th>
         <th>Tipe</th>
         <th>Kategori</th>
         <th>Jenis Pembelian</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <tr class="" id='empty_produk'>
         <td colspan="5" class="text-center">Belum Ada Produk Dipilih</td>
        </tr>
       </tbody>
      </table>
     </div>
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class='col-md-12'>
      <u>Masukkan Persyaratan</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <select id="list_syarat" error="Syarat" 
              class="form-control required"
              onchange="PembelianProduk.getDetailSyarat(this)">
               <?php if (isset($list_syarat)) { ?>
                <?php if (!empty($list_syarat)) { ?>
         <option value="">--Pilih Syarat--</option>
         <?php foreach ($list_syarat as $v_s) { ?>
          <option value="<?php echo $v_s['id'] ?>"><?php echo $v_s['syarat'] ?></option>
         <?php } ?>
        <?php } else { ?>
         <option value="">Tidak Ada Persyaratan</option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-12'>
      <u>Tanggal Jatuh Tempo</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <input type='text' name='' id='tgl_jatuh_tempo' class='form-control required' value='' error="Tanggal Jatuh Tempo"/>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id="">
       <thead>
        <tr>
         <th>Nama Syarat</th>
         <th>Upload Berkas</th>
        </tr>
       </thead>
       <tbody class="persyaratan">
        <tr class="" id='empty_syarat'>
         <td colspan="3" class="text-center">Belum Ada Persyaratan Dipilih</td>
        </tr>
       </tbody>
      </table>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="PembelianProduk.proses('<?php echo isset($id) ? $id : '' ?>')">Proses</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="PembelianProduk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
