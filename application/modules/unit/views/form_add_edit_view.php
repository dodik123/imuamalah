<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Group Satuan
     </div>
     <div class='col-md-3'>
      <select class="form-control "id="parent">
       <option value="">Pilih Group Satuan</option>
       <?php if (!empty($list_group)) { ?>
        <?php foreach ($list_group as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($parent)) { ?>
          <?php $selected = $parent == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_satuan'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Satuan
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama_satuan' class='form-control required' 
             value='<?php echo isset($nama_satuan) ? $nama_satuan : '' ?>' error="Nama Satuan"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Unit.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Unit.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
