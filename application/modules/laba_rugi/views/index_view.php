<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      <input type='text' name='' id='date' class='form-control' value='<?php echo date('Y-m-d') ?>'/>
     </div>
     <div class="col-md-3">
      <button id="" class="btn btn-success" onclick="LabaRugi.search(this)">Proses</button>
     </div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft">
        <tbody> 
         <tr class="bg-primary-light text-white">
          <th>&nbsp;</th>
          <th class="text-center"><b>Jan - Mar</b></th>
          <th class="text-center"><b>Apr - Jun</b></th>
          <th class="text-center"><b>Jul - Sep</b></th>
          <th class="text-center"><b>Okt - Des</b></th>
          <th class="text-center"><b>Total</b></th>
         </tr>
         <tr>
          <td colspan = "6" onclick="toggleContent('penjualan')" class="text-left" style="background:#cccccc;color:#555555;">Pemasukan</td>
         </tr>
         <tr role="row" class="heading penjualan">
          <td>Kas</td>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_empat'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total'], 2, ',', '.') ?></td>  
         </tr>
         <tr role="row" class="heading penjualan">
          <td class="no-sort" width="70px">Faktur (Bersyarat)</td>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_empat'], 2, ',', '.') ?></td>           
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total'], 2, ',', '.') ?></td>  
         </tr>	
         <tr role="row" class="heading penjualan">
          <td class="no-sort" width="70px">Faktur Pelanggan</td>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur_pelanggan['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur_pelanggan['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur_pelanggan['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur_pelanggan['total_empat'], 2, ',', '.') ?></td>           
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur_pelanggan['total'], 2, ',', '.') ?></td>  
         </tr>	
         <tr role="row" class="heading penjualan">
          <td class="no-sort" width="70px"><b>Laba Kotor</b></td>  
          <?php $total_lk_satu = $kas['total_satu'] + $faktur['total_satu'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_satu, 2, ',', '.') ?></td>  
          <?php $total_lk_dua = $kas['total_dua'] + $faktur['total_dua'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_dua, 2, ',', '.') ?></td>  
          <?php $total_lk_tiga = $kas['total_tiga'] + $faktur['total_tiga'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_tiga, 2, ',', '.') ?></td>  
          <?php $total_lk_empat = $kas['total_empat'] + $faktur['total_empat'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_empat, 2, ',', '.') ?></td>  
          <?php
          $total_pen = $kas['total'] + $faktur['total'] + $faktur_pelanggan['total'];
          ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_pen, 2, ',', '.') ?></td>  
         </tr>	

         <tr>
          <td colspan = "6" onclick="toggleContent('stok_keluar')" class="text-left" style="background:#cccccc;color:#555555;">Pengeluaran</td>
         </tr>
         <tr role="row" class="heading stok_keluar">
          <td class="no-sort" width="70px">Pembayaran Tagihan</td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($tagihan['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($tagihan['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($tagihan['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($tagihan['total_empat'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($tagihan['total'], 2, ',', '.') ?></td>
         </tr>	
         <tr role="row" class="heading stok_keluar">
          <td class="no-sort" width="70px">Pembayaran Vendor</td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($vendor['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($vendor['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($vendor['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($vendor['total_empat'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($vendor['total'], 2, ',', '.') ?></td>
         </tr>	
         <tr role="row" class="heading stok_keluar">
          <td class="no-sort" width="70px">Pembayaran Lain - Lain</td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_empat'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total'], 2, ',', '.') ?></td>
         </tr>
         <tr role="row" class="heading stok_keluar">
          <td class="no-sort" width="70px"><b>Total Biaya</b></td>  
          <?php $total_b_satu = $tagihan['total_satu'] + $vendor['total_satu'] + $lain['total_satu'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_satu, 2, ',', '.') ?></td>  
          <?php $total_b_dua = $tagihan['total_dua'] + $vendor['total_dua'] + $lain['total_dua'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_dua, 2, ',', '.') ?></td>  
          <?php $total_b_tiga = $tagihan['total_tiga'] + $vendor['total_tiga'] + $lain['total_tiga'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_tiga, 2, ',', '.') ?></td>  
          <?php $total_b_empat = $tagihan['total_empat'] + $vendor['total_empat'] + $lain['total_empat'] ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_empat, 2, ',', '.') ?></td>  
          <?php
          $total_peng = $tagihan['total'] + $vendor['total'] + $lain['total'];
          ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_peng, 2, ',', '.') ?></td>  
         </tr>	
         <tr role="row" class="heading">
          <td class="no-sort" width="70px"><b>Laba Bersih</b></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_satu - $total_b_satu, 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_dua - $total_b_dua, 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_tiga - $total_b_tiga, 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_empat - $total_b_empat, 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_pen - $total_peng, 2, ',', '.') ?></td>  
         </tr>	
         <tr role="row" class="heading">
          <td class="no-sort" width="70px" colspan="5"><b>Zakat</b></td>  
          <?php $total_laba = $total_pen - $total_peng; ?>
          <?php $nominal_zakat = ($total_laba * 2.5) / 100 ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($nominal_zakat, 2, ',', '.') ?></td>  
         </tr>	
         <tr role="row" class="heading">
          <td class="no-sort" width="70px" colspan="5"><b>Hasil Perhitungan Zakat (2.5 %)</b></td>  
          <?php $total_laba = $total_laba - $nominal_zakat; ?>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_laba, 2, ',', '.') ?></td>  
         </tr>	
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>
  </div>
 </div>
</div>
