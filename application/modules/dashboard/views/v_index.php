<?php echo $this->load->view('top_dashboard', array(), true); ?>


<div class="content">
 <div class="animated fadeIn">
  <div class="box">
   <div class="box-header with-border">
    <div class="row">
     <div class="col-md-10">
      <div class="box-title">
       <i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="box-body">    

    <div class='row'>
     <div class='col-md-12 text-right'>
       <span class="btn btn-danger">
        <button id="" class="btn btn-danger" onclick="Template.showUpdateSystem(this, event)">Export PDF</button>
        <label style="background-color: yellow;color:black;padding: 3px;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
     </div>
    </div>
    <br/>   

    <div class="row">
     <!-- <div class='col-md-12'>
      <h4>Grafik Penjualan (<?php echo date('F Y') ?>)</h4>
     </div> -->
     <div class="col-md-6">
      <h4>Grafik Penjualan (<?php echo $date_now ?>)</h4>
      <br/>
      <input type='hidden' name='' id='data_penjualan' class='form-control' value='<?php echo $data_penjualan['data'] ?>'/>
      <input type='hidden' name='' id='total_data_penjualan' class='form-control' value='<?php echo $data_penjualan['total'] ?>'/>
      <canvas id="canvas_pembelian"></canvas>
     </div>    
     <div class="col-md-6">
      <h4>Grafik Kredit (<?php echo $date_now ?>)</h4>
      <br/>
      <input type='hidden' name='' id='data_kredit' class='form-control' value='<?php echo $data_kredit['data'] ?>'/>
      <input type='hidden' name='' id='total_data_kredit' class='form-control' value='<?php echo $data_kredit['total'] ?>'/>
      <canvas id="canvas_kredit"></canvas>
     </div>     
    </div>  
    <br/>
    <div class='row'>
     <div class='col-md-12 text-center'>
      <label class="checkbox-inline"><input onchange="Template.showUpdateSystem(this, event)" type="checkbox" value=""> This Year</label>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <label class="checkbox-inline"><input type="checkbox" value="" onchange="Template.showUpdateSystem(this, event)"> Previous Year</label>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <label class="checkbox-inline"><input type="checkbox" value="" onchange="Template.showUpdateSystem(this, event)"> This Quarter</label>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <label class="checkbox-inline"><input type="checkbox" value="" onchange="Template.showUpdateSystem(this, event)"> Previous Quarter</label>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <label class="checkbox-inline"><input type="checkbox" value="" onchange="Template.showUpdateSystem(this, event)"> Last 12 Months</label>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <label class="checkbox-inline"><input type="checkbox" value="" onchange="Template.showUpdateSystem(this, event)"> Custom Range</label>
     </div>
    </div>
    <hr/>

    <div class='row'>
     <div class='col-md-12'>
      <h4><u>Top 5 Penjualan</u></h4>
      <br/>
      <div class='table-responsive'>
       <table class="table table-bordered">
        <thead>
         <tr class="bg-primary">
          <th>No</th>
          <th>Kode Penjualan</th>
          <th>Nama</th>
          <th>No HP</th>
          <th>Status</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($penjualan)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($penjualan as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['no_invoice'] ?></td>
            <td><?php echo $value['nama_pembeli'] ?></td>
            <td><?php echo $value['no_hp'] ?></td>
            <td><?php echo $value['status'] ?></td>
            <td class="text-center">
             <button id="" class="btn btn-succes-baru font12" 
                     onclick="Faktur.detail('<?php echo $value['id'] ?>')">Detail</button>
             &nbsp;
            </td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
