<?php

class Dashboard extends MX_Controller {

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/faktur.js"></script>'
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['penjualan'] = $this->getTopPenjualan();
  $data['total_pj'] = $this->getPenjualan();
  $data['total_pemasukan'] = Modules::run('laba_rugi/getDataFakturTotalMasuk');  
  $data['tagihan'] = Modules::run('laba_rugi/getDataTagihan');  
  $data['vendor'] = Modules::run('laba_rugi/getDataBayarVendor');
  $data['lain'] = Modules::run('laba_rugi/getDataBayarLain');
  $data['total_product'] = count($this->getTotalProduct());
  $data['total_customer'] = count($this->getTotalCustomer());
  $data['data_penjualan'] = $this->getDataPenjualanStr();
  $data['data_kredit'] = $this->getDataKredit();

  $data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
  echo Modules::run('template', $data);
 }

 public function getTotalProduct() {
  $data = Modules::run('database/get', array(
  'table' => 'product',
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getTotalCustomer() {
  $data = Modules::run('database/get', array(
  'table' => 'pembeli',
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getPenjualan() {
  $date = date('Y');
  $query = <<<QUERY
  
SELECT  
CASE 
 WHEN sp.status = 'KREDIT'
  THEN rhhjk.harga
 ELSE
  rhhjc.harga
END total
, r.product
FROM pembeli_has_product phr
JOIN product r ON phr.product = r.id
JOIN product_has_harga_jual_pokok rhhjc ON r.id = rhhjc.product AND rhhjc.period_end IS NULL
JOIN product_has_harga_jual_tunai rhhjk ON r.id = rhhjk.product AND rhhjk.period_end IS NULL
JOIN status_pembelian sp ON phr.status_pembelian = sp.id
WHERE phr.tgl_beli LIKE '%$date%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $unit = 0;
  $temp = '';
  $total = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($temp != $value['product']) {
     $unit += 1;
    }
    $total += $value['total'];
   }
  }


  return array(
  'unit'=> $unit,
  'total' => $total
  );
 }

 public function getTopPenjualan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.nama', $keyword),
   array('p.no_hp', $keyword),
   array('p.alamat', $keyword),
   array('phr.no_invoice', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_product pr',
  'field' => array('pr.*', 'phr.no_invoice', 'p.nama as nama_pembeli',
  'p.no_hp', 'p.alamat', 'sp.status'),
  'join' => array(
  array('pembeli_has_product phr', 'pr.pembeli_has_product = phr.id'),
  array('pembeli p', 'phr.pembeli = p.id'),
  array('status_pembayaran sp', 'pr.status_pembayaran = sp.id'),
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => 5,
  'orderby' => 'pr.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }


 public function getDataPenjualanStr(){
  $date = date('Y-m').'-';  
  
  $result = array();
  $total = 0;
  for($i = 1; $i < 32; $i ++){
    $i = $i < 10 ? '0'.$i : $i;
    $date_str = $date. $i;
    $query = "SELECT * FROM pembelian WHERE createddate = '".$date_str."'";

    $data = Modules::run("database/get_custom", $query);


    $result[] = empty($data) ? 0 : count($data->result_array());    
    $total += empty($data) ? 0 : count($data->result_array());    
  }    
  $str = implode(',', $result);
  return array(
    'data'=> $str,
    'total'=> $total
  );
 }
 
 public function getDataKredit(){
  $date = date('Y').'-';  
  
  $result = array();
  $total = 0;
  for($i = 1; $i < 13; $i ++){
    $i = $i < 10 ? '0'.$i : $i;
    $date_str = $date. $i;
    $query = "SELECT * FROM pembayaran_product WHERE createddate LIKE '%".$date_str."%'";

    $data = Modules::run("database/get_custom", $query);

    $result[] = empty($data) ? 0 : count($data->result_array());    
    $total += empty($data) ? 0 : count($data->result_array());    
  }    
  $str = implode(',', $result);
  return array(
    'data'=> $str,
    'total'=> $total
  );
 }

}
