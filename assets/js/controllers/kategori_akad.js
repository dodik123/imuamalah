var KategoriAkad = {
 module: function () {
  return 'kategori_akad';
 },

 add: function () {
  window.location.href = url.base_url(KategoriAkad.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(KategoriAkad.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(KategoriAkad.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(KategoriAkad.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'kategori_akad': $('#kategori_akad').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = KategoriAkad.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(KategoriAkad.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(KategoriAkad.module()) + "detail"+'/'+resp.kategori_akad;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(KategoriAkad.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(KategoriAkad.module()) + "detail/" + id;
 },
 
 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(KategoriAkad.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(KategoriAkad.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};