var JatuhTempo = {
 module: function () {
  return 'notif_jatuh_tempo';
 },

 add: function () {
  window.location.href = url.base_url(JatuhTempo.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(JatuhTempo.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(JatuhTempo.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(JatuhTempo.module()) + "index";
   }
  }
 },

 getPostDetailPengiriman: function () {
  var data = [];
  var tr = $('table').find('tbody').find('tr');
  $.each(tr, function () {
   var check = $(this).find('input#check');
   data.push({
    'id': check.attr('id_jalur'),
    'value': check.attr('value'),
    'checked': check.is(':checked')
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama_notif': $('#nama_notif').val(),
   'jadwal_notif': $('#jadwal_notif').val(),
   'jenis_notif': $('#jenis_notif').val(),
   'detail': JatuhTempo.getPostDetailPengiriman()
  };

  return data;
 },

 simpan: function (id) {
  var data = JatuhTempo.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(JatuhTempo.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(JatuhTempo.module()) + "detail" + '/' + resp.jatuh_tempo;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(JatuhTempo.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(JatuhTempo.module()) + "detail/" + id;
 },
 
 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(JatuhTempo.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(JatuhTempo.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};