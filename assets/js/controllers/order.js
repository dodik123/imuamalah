var Order = {
 module: function () {
  return 'order';
 },

 add: function () {
  window.location.href = url.base_url(Order.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Order.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Order.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Order.module()) + "index";
   }
  }
 },

 getProductItem: function () {
  var tr_data = $('table#tb_product').find('tbody').find('tr');
  var data = [];
  $.each(tr_data, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    var product_satuan = $(this).find('td:eq(0)').find('select').val();
    var qty = $(this).find('td:eq(1)').find('input').val();
    var sub_total = $.trim($(this).find('td:eq(2)').text());
    var id = $(this).attr('data_id');

    var bank = "";
//    if (metode == 2) {
//     bank = $(this).closest('tr').next().attr('data_bank');
//    }

    data.push({
     'id': id,
     'product_satuan': product_satuan,
//     'pajak': pajak,
//     'metode': metode,
     'qty': qty,
     'sub_total': sub_total,
//     'bank': bank,
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'pembeli': $('#pembeli').val(),
   'tanggal_faktur': $('#tanggal_faktur').val(),
   'total': $('label#total').text(),
   'product_item': Order.getProductItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = Order.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Order.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Order.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Order.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Order.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Order.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Order.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $("#product").select2();
  $("#pembeli").select2();
  $("#metode").select2();
  $("#pajak").select2();

  var tr_product = $('table#tb_product').find('tbody').find('tr');
  $.each(tr_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    var data_id = $(this).attr('data_id');
    if (data_id != '') {
     var index = $(this).index();
     $(this).find('td:eq(0)').find('select').select2();
     $(this).find('td:eq(1)').find('select').select2();
     $(this).find('td:eq(2)').find('select').select2();
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_faktur').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
  $('input#tanggal_bayar').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: tr.index()
   },
   async: false,
   url: url.base_url(Order.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 hitungSubTotal: function (elm, jenis) {
  var tr = $(elm).closest('tr');

  var option_product = tr.find('td:eq(0)').find('select').find('option');
  var harga = 0;
  $.each(option_product, function () {
   if ($(this).is(':selected')) {
    harga = $(this).attr('harga');
   }
  });

//  var option_pajak = tr.find('td:eq(1)').find('select').find('option');
  var persentase = 0;
//  $.each(option_pajak, function () {
//   if ($(this).is(':selected')) {
//    persentase = $(this).attr('persentase');
//   }
//  });

  var jumlah = parseInt(tr.find('td:eq(1)').find('input').val());
  var sub_total_content = tr.find('td:eq(2)');

//  var potong_pajak = (jumlah * harga * persentase) / 100;
//  var sub_total = (jumlah * harga) - potong_pajak;
  var sub_total = (jumlah * harga);
  sub_total_content.find('label#sub_total').text(sub_total);

  Order.hitungTotal();
 },

 hitungTotal: function () {
  var tb_product = $('table#tb_product').find('tbody').find('tr');
//  console.log(tb_product);
  var total = 0;
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var sub_total = $(this).find('td:eq(2)').text().toString();
     sub_total = sub_total.replace(',', '');
     sub_total = parseInt(sub_total);
     total += sub_total;
    }
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   var metode = tr.find('td:eq(2)').find('select').val();
   tr.addClass('hide');
   tr.addClass('deleted');
   if (metode == 2) {
    tr.next().addClass('hide');
    tr.next().addClass('deleted');
   }
  } else {
   $(elm).closest('tr').remove();
  }

  Order.hitungTotal();
 },

 getMetodeBayar: function (elm) {
  var metode = $(elm).val();
  var index = $(elm).closest('tr').index();
  switch (metode) {
   case "2":
    $.ajax({
     type: 'POST',
     data: {
      metode: metode,
      index: index
     },
     dataType: 'html',
     async: false,
     url: url.base_url(Order.module()) + "getMetodeBayar",
     error: function () {
      toastr.error("Gagal");
     },

     beforeSend: function () {

     },

     success: function (resp) {
      bootbox.dialog({
       message: resp
      });
     }
    });
    break;

   default:

    break;
  }
 },

 pilihBank: function (elm) {
  message.closeDialog();
  var index_row = $(elm).attr('index_row');
  var tb_product = $('table#tb_product').find('tbody');
  var tr_product = tb_product.find('tr:eq(' + index_row + ')');

  var nama_bank = $(elm).closest('tr').find('td:eq(0)').text();
  var akun = $(elm).closest('tr').find('td:eq(2)').text();
  var no_rek = $(elm).closest('tr').find('td:eq(1)').text();
  var data_id_bank = $(elm).closest('tr').attr('data_id');
  var tr_bank = '<tr data_bank="' + data_id_bank + '">';
  tr_bank += '<td colspan="7">';
  tr_bank += '[' + nama_bank + '] - [' + akun + '] - [' + no_rek + ']';
  tr_bank += '</td>';
  tr_bank += '</tr>';
  tr_product.after(tr_bank);
 },

 cetak: function (id) {
  window.open(url.base_url(Order.module()) + "printFaktur/" + id);
 },

 bayar: function () {
  window.location.href = url.base_url("payment") + "add";
 },

 cancel: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    order_id: id,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Order.module()) + "cancelOrder",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dibatalkan");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dibatalkan");
    }
   }
  });
 }
};

$(function () {
 Order.setDate();
 Order.setSelect2();
 Order.setThousandSparator();
});