//import firebase from 'firebase';
//import 'firebase/storage'; 

var LacakSalesman = {
 map: '',
 module: function () {
  return 'lacak_salesman';
 },
 initSDK: function () {
  console.log('SDK berhasil');
 },
 initMap: function (lat, lng) {
  LacakSalesman.map = new google.maps.Map(
          document.getElementById('map'), {zoom: 4, center: {lat: lat, lng: lng}});
 },
 initMarkers: function (marker) {
  let latLng = {lat: parseInt(marker.lat), lng: parseInt(marker.lng)};
  let markerIcon = {
   url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
   scaledSize: new google.maps.Size(80, 80),
   origin: new google.maps.Point(0, 0),
   anchor: new google.maps.Point(32, 65),
   labelOrigin: new google.maps.Point(40, 33),
  };

  let temp_marker = new google.maps.Marker({
   position: latLng,
   animation: google.maps.Animation.DROP,
   icon: markerIcon,
   label: {
    text: marker.id,
    color: "#eb3a44",
    fontSize: "12px",
    fontWeight: "bold"
   },
   map: LacakSalesman.map
  });
  return temp_marker;

 },
 updateMarker: function (id, marker) {
  LacakSalesman.getUserLocation(id, function (resp) {
   console.log('update loc', id);
   if (resp.is_valid) {
    let latLng = new google.maps.LatLng(parseInt(resp.result[0].lat), parseInt(resp.result[0].lng));
    marker.setPosition(latLng);
    // loop
    setTimeout(function () {
     LacakSalesman.updateMarker(id, marker)
    }, 3000);
   }
  });
 },
 initSalesLocation: function (id) {
  LacakSalesman.getUserLocation(id, function (resp) {
//   console.log(resp);

   if (resp.is_valid) {
    toastr.success("Berhasil Menampilkan Lokasi User");
    // clear last time out
    var _id = window.setTimeout(function () {}, 0);
    while (_id--) {
     window.clearTimeout(_id); // will do nothing if no timeout with id is present
    }

    let view = resp.result[0];
    LacakSalesman.initMap(parseInt(view.lat), parseInt(view.lng));
    // load markers & update
    resp.result.map((marker) => {
     let markerUser = LacakSalesman.initMarkers(marker);
     LacakSalesman.updateMarker(id, markerUser);
    });

   } else {
    toastr.error("Gagal Menampilkan Lokasi User");
   }
  });

 },
 getUserLocation: function (id, callback) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(LacakSalesman.module()) + "getLogUserLocation/" + id,

   error: function () {
    callback({is_valid: false});
   },

   success: function (resp) {
    callback(resp);
   }
  });
 },

 initDataPosition: function (position) {
  var lat = $.trim(position.lat);
  var lng = $.trim(position.lng);
  console.log(position);
  LacakSalesman.initMap(parseInt(lat), parseInt(lng));

  let marker = {
   'lat': parseInt(lat),
   'lng': parseInt(lng)
  };
  LacakSalesman.initMarkers(marker);
 },

 setSelect2: function () {
  $('select#salesman').select2();
 },

 getPositionSales: function () {
  var firebaseConfig = {
   apiKey: "AIzaSyBA38RD-k_JThXLMAe5mp0KTHYgtMpDFfM",
   authDomain: "messaging-a4d69.firebaseapp.com",
   databaseURL: "https://messaging-a4d69.firebaseio.com",
   projectId: "messaging-a4d69",
   storageBucket: "messaging-a4d69.appspot.com",
   messagingSenderId: "191503415036",
// appID: "app-id",
  };

  var sales_id = $('select#salesman').val();
  let init_fb = firebase.initializeApp(firebaseConfig);
  let ref = firebase.database().ref().child("user-" + sales_id);
  let data_posisi = "";
  ref.on('value', snap => LacakSalesman.initDataPosition(snap.val()));
 },

 goPositionSales: function (elm) {
  var sales = $(elm).val();
  if(sales != ''){
   window.location.href = url.base_url(LacakSalesman.module()) + "index/" + sales;
  }else{
   window.location.href = url.base_url(LacakSalesman.module()) + "index";
  }  
 }
};

$(function () {
 LacakSalesman.setSelect2();
 var sales_id = $('input#sales_id').val();
 if (sales_id != '0') {
  LacakSalesman.getPositionSales();
 }
// ref.on('value', snap => console.log(snap.val()));

// $('select#salesman').on('change', function (e) {
//  let id = $(this).find("option:selected").val();
//  LacakSalesman.initSalesLocation(id);
// });
})