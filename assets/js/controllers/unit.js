var Unit = {
 module: function () {
  return 'unit';
 },

 add: function () {
  window.location.href = url.base_url(Unit.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Unit.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Unit.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Unit.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'parent': $('#parent').val(),
   'nama_satuan': $('#nama_satuan').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Unit.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Unit.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Unit.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Unit.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Unit.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Unit.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Unit.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setSelect2: function(){
  $('select#parent').select2();
 }
};

$(function () {
 Unit.setSelect2();
 Unit.setThousandSparator();
});