var Faktur = {
 module: function () {
  return 'faktur';
 },

 add: function () {
  window.location.href = url.base_url(Faktur.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Faktur.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Faktur.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Faktur.module()) + "index";
   }
  }
 },

 getPostData: function (elm) {
  var tr = $(elm).closest('tr');
  var data = {
   'pembelian_rumah': tr.attr('id'),
   'status': $.trim(tr.attr('status')).toLowerCase(),
   'pembayaran_rumah_id': $.trim(tr.attr('pembayaran_rumah')),
   'jumlah_bayar': $.trim(tr.find('input#jumlah_bayar').val()),
   'tgl_bayar': $.trim(tr.find('input#tgl_bayar').val()),
   'belum_dicicil': $.trim(tr.find('b#belum_dicicil').text()),
   'sudah_dicicil': $.trim(tr.find('b#sudah_dicicil').text())
  };

  return data;
 },

 prosesBayar: function (elm) {
  var data = Faktur.getPostData(elm);

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Faktur.module()) + "prosesBayar",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Faktur.module()) + "detail" + '/' + resp.rumah_bayar;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 bayarInvoice: function (id) {
  window.location.href = url.base_url(Faktur.module()) + "bayar/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Faktur.module()) + "detail/" + id;
 },

 getInvocePembelian: function (elm, e) {
  if (e.keyCode == 13) {
   var invoice = $.trim($(elm).val());
   if (invoice == '') {
    toastr.error("Invoice Harus Diisi");
   } else {
    //search data invoice pembelian
    $.ajax({
     type: 'POST',
     data: {
      no_invoice: invoice
     },
     dataType: 'html',
     async: false,
     url: url.base_url(Faktur.module()) + "getInvocePembelian",
     beforeSend: function () {
      message.loadingProses('Proses Retrieving Data...');
     },

     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     success: function (resp) {
      $('div.content_data_pembelian').html(resp);
      Faktur.setThousandSparator();
      message.closeLoading();
     }
    });
   }
  }
 },

 bayar: function (id) {
  var formBayar = $('tr.content_' + id);
  if (formBayar.hasClass('hide')) {
   formBayar.removeClass('hide');
   formBayar.find('input#tgl_bayar').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true
   });
   formBayar.find('input#jumlah_bayar').val(0);
   formBayar.find('input#tgl_bayar').val('');
  } else {
   formBayar.addClass('hide');
  }
 },

 cancelBayar: function (elm) {
  $(elm).closest('tr').addClass('hide');
  $(elm).closest('tr').find('input#jumlah_bayar').val(0);
  $(elm).closest('tr').find('input#tgl_bayar').val('');
 },

 printFaktur: function (elm) {
  var tbody = $('#table_angsuran').find('tbody').find('tr:last');
  var no_invoice = $.trim(tbody.find('td:eq(0)').text());
  var id_faktur = $('#id_faktur').val();
  window.open(url.base_url(Faktur.module()) + "printFaktur/" + no_invoice + "/" + id_faktur);
 },
 
 setThousandSparator: function(){
  $('#jumlah_bayar').divide({
   delimiter: '.',
   divideThousand: true
  });
 }
};

$(function(){
 Faktur.setThousandSparator();
});