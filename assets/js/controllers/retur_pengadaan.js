var ReturPengadaan = {
 module: function () {
  return 'retur_pengadaan';
 },

 add: function () {
  window.location.href = url.base_url(ReturPengadaan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(ReturPengadaan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(ReturPengadaan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ReturPengadaan.module()) + "index";
   }
  }
 },

 getPostProcItem: function () {
  var data = [];
  var tb_product = $('table#tb_product').find('tbody').find('tr');
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var data_id = $(this).attr('data_id');
     var checked = $(this).find('input#check');
     if (checked.is(':checked')) {
      data.push({
       'proc_item': data_id,
       'jumlah': $(this).find('td:eq(3)').find('input').val(),
       'sub_total': $(this).find('td:eq(4)').text(),
      });
     }
    }
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'procurement': $('#procurement').val(),
   'tanggal': $('#tanggal_retur').val(),
   'keterangan': $('#keterangan_retur').val(),
   'total': $('label#total').text(),
   'retur_item': ReturPengadaan.getPostProcItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = ReturPengadaan.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ReturPengadaan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ReturPengadaan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(ReturPengadaan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(ReturPengadaan.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ReturPengadaan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ReturPengadaan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $('select#procurement').select2();
 },

 setDate: function () {
  $('input#tanggal_retur').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');

  var index = tr.index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(ReturPengadaan.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.addClass('item-' + index);
    tr.addClass('_data_item');
    tr.attr('urutan', index);
    tr.after(newTr);
   }
  });
 },

 hitungSubTotal: function (elm, jenis) {
  var tr = $(elm).closest('tr');
  var harga = parseInt(tr.find('td:eq(2)').attr('harga'));

  var jumlah = parseInt(tr.find('td:eq(3)').find('input').val());
  var sub_total_content = tr.find('td:eq(4)');
  console.log(sub_total_content);
  var sub_total = (jumlah * harga);
  sub_total_content.text(sub_total);
  ReturPengadaan.hitungTotal();
 },

 addSatuanContent: function (elm) {
  var tr_current = $(elm).closest('tr');
  var urutan = tr_current.attr('urutan');

  var tr_last = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    tr_index: tr_last.index(),
    urutan: urutan
   },
   async: false,
   url: url.base_url(ReturPengadaan.module()) + "addSatuanContent",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var newTr = tr_current.clone();
    newTr.html(resp);
    newTr.find('td:eq(1)').find('select').select2();
    newTr.addClass('children');
    tr_current.after(newTr);
   }
  });
 },

 removeSatuanContent: function (elm) {
  $(elm).closest('div.select_content').remove();
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   tr.addClass('hide');
   tr.addClass('deleted');
  } else {
   $(elm).closest('tr').remove();
  }

  ReturPengadaan.hitungTotal();
 },

 hitungTotal: function () {
  var tb_product = $('table#tb_product').find('tbody').find('tr');
  var total = 0;
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var sub_total = $(this).find('td:eq(4)').text().toString();
//     sub_total = sub_total.replace(',', '');
     sub_total = sub_total.replace(/,/g, '');
     sub_total = parseInt(sub_total);
     total += sub_total;
    }
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 cetak: function (id) {
  window.open(url.base_url(ReturPengadaan.module()) + "printFaktur/" + id);
 },

 getPengadaanDetail: function (elm) {
  var pengadaan = $(elm).val();

  $.ajax({
   type: 'POST',
   data: {
    procurement: pengadaan
   },
   dataType: 'json',
   async: false,
   url: url.base_url(ReturPengadaan.module()) + "getPengadaanDetail",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('input#vendor').val(resp.data.nama_vendor);
    $('input#tanggal').val(resp.data.tanggal);
    $('label#total').text(resp.data.total);
    $('#keterangan').val(resp.data.keterangan);
    $('div.form-item').html(resp.view_item);
   }
  });
 },

 hitungSubTotalRetur: function (elm) {
  var tr = $(elm).closest('tbody').find('tr');
  var total = 0;
  $.each(tr, function () {
   var check = $(this).find('input#check');
   if (check.is(':checked')) {
    var sub_total = $(this).find('td:eq(4)').text().toString();
//     sub_total = sub_total.replace(',', '');
    sub_total = sub_total.replace(/,/g, '');
    sub_total = parseInt(sub_total);
    total += sub_total;
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 }
};

$(function () {
 ReturPengadaan.setDate();
 ReturPengadaan.setSelect2();
 ReturPengadaan.setThousandSparator();
});